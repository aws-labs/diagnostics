create if not exists database testdb;
create if not exists table users (
    id int auto_increment primary key,
    nazwa varchar(20) not null,
    opis varchar(128) null
);

create if not exists table testtable (
    id int auto_increment primary key,
    nazwa varchar(20) not null
)

create if not exists table languages (
    id int auto_increment primary key,
    nazwa varchar(20) not null,
    id_jez varchar(7) not null,
    lang_col varchar(7) not null
)