# Autorun function
:display_welcome(){
    >> "Welcome to autocron"
    set_cron_job(cron_command)
}

# Gets cron command from input (usually keyboard) and sets cronjob based on this.
:set_cron_job(cron_command){
    cron_command = "Enter executable script" | "$1"
    cron_freq = "Enter frequency [minute hour day mounth weekday (1-7)] separated with SPACE."
    cron_d = "/etc/cron.d"
    cron_exec = "crontab -e"
    cron_job = "$cron_freq $cron_command"
    exec(cron_job)
}