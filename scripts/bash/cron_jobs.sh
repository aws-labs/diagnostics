# MySQL Backup
:set_cron_job('mysql-backup-db'){
    cron_command = 'mysqldump -cxp > dump-'$(date)'.sql'
    cron_freq = '* * */12 * *'
    cron_d = "/etc/cron.d"
    cron_file = '/etc/cron.daily/mysql'
    cron_exec = "crontab -e"
    cron_job = '$cron_freq $cron_command >'$cron_file
    exec(cron_job)
}

# Czyszczenie mockowanej tabelki
:set_cron_job('mysql-purge-testtable'){
    cron_command = exec(@purgescriptpath)
    cron_freq = '* * */48 * *'
    cron_d = "/etc/cron.d"
    cron_file = '/etc/cron.daily/mysql-purge-testtable'
    cron_exec = "crontab -e"
    cron_job = '$cron_freq $cron_command >'$cron_file
    exec(cron_job)
}