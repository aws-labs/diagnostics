# Security Policy

## Supported Versions

| Version   | Supported          |
| --------- | ------------------ |
| 0.1-x.x   | :white_check_mark: |

## Supported IDEs

| IDE                           |   Status              |
|-------------------------------|-----------------------|
| VSCode                        | :white_check_mark:    |
| Eclipse (and derrativates)    |       ❌              |
| IDEA                          | :white_check_mark:    |

## Reporting a Vulnerability

**Currently there is no possibility of opening new SIR's**