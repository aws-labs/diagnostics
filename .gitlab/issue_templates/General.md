## Co?
<!--Krótki opis Issue -->

## Aktualne zachowanie
<!-- Dokładnie opisz o co chodzi w danym Issue -->

## Oczekiwane zachowanie
<!-- Jakie powinno być oczekiwane zachowanie -->

## Kroki do odtworzenia działania
<!-- Jakie kroki doprowadziły do obecnego zachowania aplikacji? -->

## Szczegółowe parametry systemu
<!-- CPU/RAM/OS oraz inne technikalia -->