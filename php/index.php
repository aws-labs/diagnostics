<?php
/*
* Prosty framework szkieletowy do testowania skryptów PHP.
* @author: macfanpl
* @version: (tag-based)
*/
require 'inc/diagnostics.php';
require 'inc/functions.php';
require '../conf/polygon/php/conf_defines.php';

// Zainicjowanie poszczególnych klas
$ac = new diagnostics();
$f = new addedFunctions();

if (DEBUG_MODE == 1) {
    $f->callDebugger();
} elseif (DEBUG_MODE == 0) {
    $ac->runAllRecursively();
}

?>