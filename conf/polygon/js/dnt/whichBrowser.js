// Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
// @ts-ignore
var isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
// @ts-ignore
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof isSafari !== 'undefined' && isSafari.pushNotification));

// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.DOCUMENT_NODE;

// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 71
var isChrome = !!window.isChrome && (!!window.isChrome.webstore || !!window.isChrome.runtime);

// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;
var output = 'Detecting browsers by ducktyping:<hr>';
/*output += 'isFirefox: ' + isFirefox + '<br>';
output += 'isChrome: ' + isChrome + '<br>';
output += 'isSafari: ' + isSafari + '<br>';
output += 'isOpera: ' + isOpera + '<br>';
output += 'isIE: ' + isIE + '<br>';
output += 'isEdge: ' + isEdge + '<br>';
output += 'isBlink: ' + isBlink + '<br>';*/

switch (output) {
    case 'isFirefox':
        isFirefox
        break;

    case 'isChrome':
        isChrome
        break;

    case 'isSafari':
        isSafari
        break;

    case 'isOpera':
        isOpera
        break;

    case 'isIE':
        isIE
        break;

    case 'isEdge':
        isEdge
        break;

    case 'isBlink':
        isBlink
        break;

    default:
        break;
}

// Prints output
document.body.innerHTML = output;