export var dnt = doNotTrack;
export var dnt_standard_cookie;
export var dnt_adv_cookie;
export var rozmiar;

switch (dnt) {
    case 'dnt_standard':
        dnt_standard_cookie = document.cookie("dntType=standard; expires=Sun, 18 Nov 2020 12:00:00 UTC; path=/");
        console.info("Standard DNT enabled");
        break;

    case 'dnt-adv':
        // @ts-ignore
        dnt_standard_cookie = document.cookie("dntType=advanced; expires=Sun, 18 Nov 2020 12:00:00 UTC; path=/");
        console.log('Extended DNT enabled');
        break;

    case 'language-css-info':
        document.body.style.borderImage = 'none';
}