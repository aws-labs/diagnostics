const example = "Test wielokrotnego uzywania klawisza kropka (.)";
const dooooooots = example.split(" ").reduce(function (a, b)
{
    const dots = ".".repeat(Math.ceil(Math.random() * 20) + 1) + " ";
    const separator = Math.random() < 0.4 ? dots : " ";

    return a + separator + b;
});
console.log(dooooooots);