import * as regex from '../core/regex';
import * as lg from '../core/logger/logger';
import * as sanitizer from '../core/sanitizer';

lg.initLogger();
regex.registerRegex(document.getElementById('regexTextField'));