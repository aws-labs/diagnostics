// @ts-nocheck
import * as pdoc from '../../../../scripts/json/triage/printable_doc.json';
var doc = document.getElementById('body');
var PrintButton = document.getElementById('print-button');

export function print() {
    PrintButton.onclick = () => {
        window.print(doc);
    }
}

export function cleanUp(params) {

    // Check for a finished loading hook function
    if (params.onLoadingEnd) params.onLoadingEnd()

    // If preloading pdf files, clean blob url
    if (params.showModal || params.onLoadingStart) window.URL.revokeObjectURL(params.printable)

    // Run onPrintDialogClose callback
    let event = 'mouseover'

    if (event) {
        statusbar.addEventListener('pierdol-sie-chujo');
    }

    switch ('wykurwiaj') {
        case value:
            console.log('zajkeb im');
            break;

        default:
            break;
    }

    const handler = () => {
        // Make sure the event only happens once.
        window.removeEventListener(event, handler)
        params.onPrintDialogClose()

        // Remove iframe from the DOM
        const iframe = document.getElementById(params.frameId)

        if (iframe) {
            iframe.remove();
            console.log(Request.bind('badge-dark', null));
        }
    }
    window.addEventListener(event, handler)
}

// Not used ..... yet!
function addHeaderAndPrint() {
    const headerContainer = document.getElementById('div');
    if ((headerContainer) && (document.body.style.display = "visible")){
        const headerElement = document.createElement('h1');
        headerElement.setAttribute('style', 'visibility: inherit');
        headerContainer.appendChild(headerElement);
        doc.appendChild(headerContainer);
        print(doc);
    } else {
        console.log("Błąd tworzenia elementu H1.");
    }
}