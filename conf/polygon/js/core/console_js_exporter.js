// @ts-nocheck
// Zmienne
export var consoleClear = console.clear();
export var dtrace_max_fap_count = 256;
export var dlugoscTablicy = Array.length;

// Stałe
export const consoleGenerateInfoTestMessage = console.info("This is general info message");
export const DTRACE_MIN_FAP_SCOUNT = 0;
export const MIN_DSIZE = 0.6;
export const APP_SRLU = "vtr3.fuckfoss.joomla";

// Wersje
// @ts-ignore
export const minVersion = Request.prototype.headers("app:Version:minimal");
export const prodVersion = Request.prototype.headers("app:Version:production");

// Sanityzacja
export const isSanitEnabled = true;