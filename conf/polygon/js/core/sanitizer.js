export function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}

export function encodeID(s) {
    if (s === '') return '_';
    return s.replace(/[^a-zA-Z0-9.-]/g, function (match) {
        return '_' + match[0].charCodeAt(0).toString(16) + '_';
    });
}

function addChut(user_id) {
    var log = document.createElement('div');
    log.className = 'log';
    var textarea = document.createElement('textarea');
    var input = document.createElement('input');
    input.value = user_id;
 //   input.readonly = True;
    var button = document.createElement('input');
    button.type = 'button';
    button.value = 'Message';

    // Chut = div
    var chut = document.createElement('div');
    chut.className = 'chut';
    chut.appendChild(log);
    chut.appendChild(textarea);
    chut.appendChild(input);
    chut.appendChild(button);
    document.getElementById('chuts').appendChild(chut);

    button.onclick = function () {
        var alertmsg = "Send" + textarea.value + " to " + user_id;
        console.log(alertmsg);
    };

    return chut;
}