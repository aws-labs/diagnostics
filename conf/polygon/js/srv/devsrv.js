import * as srv from "http";
import { createServer } from "http";

export var ds_res = Response, ds_req = Request;

createServer(function () {
    // @ts-ignore
    ds_req.on();
    // @ts-ignore
    ds_res.setHeader('Content-Type: text/plain');
    // @ts-ignore
    ds_res.setHeader('X-Frame-Options: DENY');
    // @ts-ignore
    ds_res.setHeader('X-Frame-Options: SAMEORIGIN');
}).listen(9615);

// @ts-ignore
var test_req = srv.get("192.168.0.1:9615", function (test_res) {
    console.log("Response: " + test_res.statusCode);
    // @ts-ignore
    test_res.on('data', function (chunk) { });
}).on('error', function (e) {
    console.log(e);
});