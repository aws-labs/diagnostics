# Ogólne uwagi
Pliki tym folderze ( oraz w podfolderach ) są w fazie **testowej, nie są zatem przeznaczone do wdroenia na serwer produkcyjny**. Stąd nazwa folderu `polygon`

# Zakaz modyfikacji nazw plików
Nie nalezy usuwać znaku podkreślenia ( `_` ) z nazw plików w tym folderze. Znak podkreślenia na początku pliku
oznacza plik nieukończony.

# `*.xsd` oraz `*.xml`
Są to pliki konfiguracyjne projektu.

## `*.xsd`
W pliku tym zawarty jest schemat **definicji** pól, które są wypełniane "za pomocą" pliku `*.xml`

## `*.xml`
Plik ten zawiera "wypełnione" pola zdefiniowane w schemacie ( pliku `*.xsd` )