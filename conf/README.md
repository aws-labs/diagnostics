## Uwagi do plików oraz podfolderów obecnych w katalogu `/conf`

# `hardware_conf`

Przechowuje konfiguracje poszczególnych urządzeń wchodzących w skład infrastruktury projektu.

Dzięki zastosowaniu `zmiennych globalnych` do autoryzacji uzytkownika, **nie jest wymagane** podejmowanie zadnej akcji. Skrypt automatycznie pobierze nasze dane autoryzacyjne z konfiguracji systemowej i wykorzysta je do autoryzacji na serwerze repozytorium.

# `polygon`

Przechowuje:
* wszelkie pliki **nie nadające się** na produkcję ( zarówno w fazie `alpha` jak i `beta`),
* konfigurację projektu,
* submoduły projektu

## Róznica pomiędzy `dependencies` a `submodules`

### `dependencies`
* są to projekty ( umieszczone nie koniecznie jako `vcs`), których źródła **nie są** dostępne w drzewie katalogów projektu-matki,
* nowa wersja ( `release` ) projektu będącego zaleznością **nie powoduje** automatycznego wykorzystania jej w projekcie-matce ( jest wymagany numer konkretnej uzywanej wersji ),
* komenda `git pull` wykonana na repozytorium projektu-matki, **nie powoduje** zaviągnięcia zalezności.

### `submodules`
* są to projekty posiadające źródła jako `vcs`,
* nowa wersja ( `release` ) projektu będącego zaleznością **powoduje** automatyczne wykorzystanie jej w projekcie-matce ( numer wersji **nie ma** zadnego znaczenia, zawsze uzywana jest najnowsza ( zgodnie z `release` )),
* komenda `git pull` wykonana na repozytorium projektu-matki, **nie powoduje** zaciągnięcia submodułu, aby go pobrać, nalezy wykonac:
```bash
git clone --recurse-submodules http://adres.com/repo.git katalog-docelowy
```
a następnie
```bash
cd katalog-docelowy
git submodule init
git submodule update
```


# Informacja odnośnie submodułów projektu ( katalog `polygon/submodules` )

**Nie wprowadzamy** zadnych zmian w następujących katalogach:
```bash
submodules/
submodules/moz-observatory
submodules/tiny
submodules/jsbeautify
submodules/jqueryjs
```
Są to jedynie `klony` właściwych repozytoriów ( do których **nie posiadamy** uprawnień typu `git commit` ) i **kazda** próba skończy się komunikatem o braku uprawnień.